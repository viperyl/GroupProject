#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/manas/check_robot/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/manas/check_robot/build'
export ROSLISP_PACKAGE_DIRECTORIES="/home/manas/check_robot/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/manas/check_robot/src:$ROS_PACKAGE_PATH"