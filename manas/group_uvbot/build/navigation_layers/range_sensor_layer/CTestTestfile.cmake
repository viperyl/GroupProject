# CMake generated Testfile for 
# Source directory: /home/manas/group_uvbot/src/navigation_layers/range_sensor_layer
# Build directory: /home/manas/group_uvbot/build/navigation_layers/range_sensor_layer
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_range_sensor_layer_roslint_package "/home/manas/group_uvbot/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/manas/group_uvbot/build/test_results/range_sensor_layer/roslint-range_sensor_layer.xml" "--working-dir" "/home/manas/group_uvbot/build/navigation_layers/range_sensor_layer" "--return-code" "/opt/ros/melodic/share/roslint/cmake/../../../lib/roslint/test_wrapper /home/manas/group_uvbot/build/test_results/range_sensor_layer/roslint-range_sensor_layer.xml make roslint_range_sensor_layer")
