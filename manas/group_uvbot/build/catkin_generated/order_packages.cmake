# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "image_transport_plugins")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "image_transport_plugins/image_transport_plugins")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "navigation_layers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation_layers/navigation_layers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "opencv_tests")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "vision_opencv/opencv_tests")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_arduino")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_arduino")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_mbed")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_mbed")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_python")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_python")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_tivac")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_tivac")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_vex_cortex")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_vex_cortex")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_vex_v5")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_vex_v5")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_xbee")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_xbee")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_client")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_client")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "serial")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "serial")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "aruco_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "aruco_ros/aruco_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "hector_map_tools")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "hector_slam/hector_map_tools")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "vision_opencv")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "vision_opencv/vision_opencv")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "world_canvas_server")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "world_canvas/world_canvas_server")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "app_node")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "uv_robots/robot-app-node")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "map2gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "map2gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "map_server")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation/map_server")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_server")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_server")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "cv_bridge")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "vision_opencv/cv_bridge")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "aruco")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "aruco_ros/aruco")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "image_geometry")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "vision_opencv/image_geometry")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "compressed_depth_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "image_transport_plugins/compressed_depth_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "compressed_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "image_transport_plugins/compressed_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "hector_compressed_map_transport")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "hector_slam/hector_compressed_map_transport")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "lidar_filter")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "lidar_filter")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "aruco_ros")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "aruco_ros/aruco_ros")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "robot_pose_publisher")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "robot_pose_publisher")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_embeddedlinux")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_embeddedlinux")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_test")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_test")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosserial_windows")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosserial/rosserial_windows")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "theora_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "image_transport_plugins/theora_image_transport")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "octomap_rviz_plugins")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "octomap_rviz_plugins")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "uvrobots_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "uvrobots/uvrobots_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "uvrobots_nav")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "uvrobots/uvrobots_nav")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "uvrobots_simulations")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "uvrobots/uvrobots_simulations")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "range_sensor_layer")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation_layers/range_sensor_layer")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "social_navigation_layers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "navigation_layers/social_navigation_layers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ydlidar_ros")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "ydlidar_ros")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "zephrus")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "zephrus")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python2.7/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
