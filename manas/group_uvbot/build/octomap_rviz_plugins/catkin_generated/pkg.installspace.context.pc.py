# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include;/opt/ros/melodic/include".split(';') if "${prefix}/include;/opt/ros/melodic/include" != "" else []
PROJECT_CATKIN_DEPENDS = "octomap_msgs;roscpp;rviz".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-loctomap_rviz_plugins;/opt/ros/melodic/lib/liboctomap.so;/opt/ros/melodic/lib/liboctomath.so".split(';') if "-loctomap_rviz_plugins;/opt/ros/melodic/lib/liboctomap.so;/opt/ros/melodic/lib/liboctomath.so" != "" else []
PROJECT_NAME = "octomap_rviz_plugins"
PROJECT_SPACE_DIR = "/home/manas/group_uvbot/install"
PROJECT_VERSION = "0.2.3"
