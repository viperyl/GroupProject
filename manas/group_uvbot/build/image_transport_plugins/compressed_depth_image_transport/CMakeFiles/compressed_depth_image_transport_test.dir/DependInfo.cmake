# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/src/codec.cpp" "/home/manas/group_uvbot/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/src/codec.cpp.o"
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/src/compressed_depth_publisher.cpp" "/home/manas/group_uvbot/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/src/compressed_depth_publisher.cpp.o"
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/src/compressed_depth_subscriber.cpp" "/home/manas/group_uvbot/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/src/compressed_depth_subscriber.cpp.o"
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/src/manifest.cpp" "/home/manas/group_uvbot/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/src/manifest.cpp.o"
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/src/rvl_codec.cpp" "/home/manas/group_uvbot/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/src/rvl_codec.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"compressed_depth_image_transport\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/manas/group_uvbot/devel/include"
  "/home/manas/group_uvbot/src/image_transport_plugins/compressed_depth_image_transport/include"
  "/home/manas/group_uvbot/src/vision_opencv/cv_bridge/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/manas/group_uvbot/build/vision_opencv/cv_bridge/src/CMakeFiles/cv_bridge.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
