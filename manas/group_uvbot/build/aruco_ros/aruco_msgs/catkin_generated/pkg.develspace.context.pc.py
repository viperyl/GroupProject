# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/manas/group_uvbot/devel/include".split(';') if "/home/manas/group_uvbot/devel/include" != "" else []
PROJECT_CATKIN_DEPENDS = "message_runtime;geometry_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "aruco_msgs"
PROJECT_SPACE_DIR = "/home/manas/group_uvbot/devel"
PROJECT_VERSION = "2.1.1"
