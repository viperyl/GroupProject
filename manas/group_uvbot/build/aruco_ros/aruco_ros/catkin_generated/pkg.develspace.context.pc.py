# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/manas/group_uvbot/devel/include;/home/manas/group_uvbot/src/aruco_ros/aruco_ros/include".split(';') if "/home/manas/group_uvbot/devel/include;/home/manas/group_uvbot/src/aruco_ros/aruco_ros/include" != "" else []
PROJECT_CATKIN_DEPENDS = "aruco;sensor_msgs;tf".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-laruco_ros_utils".split(';') if "-laruco_ros_utils" != "" else []
PROJECT_NAME = "aruco_ros"
PROJECT_SPACE_DIR = "/home/manas/group_uvbot/devel"
PROJECT_VERSION = "2.1.1"
