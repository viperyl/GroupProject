# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/cameraparameters.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/cameraparameters.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/cvdrawingutils.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/cvdrawingutils.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/debug.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/debug.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/dictionary.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/dictionary.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/ippe.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/ippe.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/marker.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/marker.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/markerdetector.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/markerdetector.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/markerlabeler.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/markerlabeler.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/markerlabelers/dictionary_based.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/markerlabelers/dictionary_based.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/markerlabelers/svmmarkers.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/markerlabelers/svmmarkers.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/markermap.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/markermap.cpp.o"
  "/home/manas/group_uvbot/src/aruco_ros/aruco/src/aruco/posetracker.cpp" "/home/manas/group_uvbot/build/aruco_ros/aruco/CMakeFiles/aruco.dir/src/aruco/posetracker.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/manas/group_uvbot/src/aruco_ros/aruco/include/aruco"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
