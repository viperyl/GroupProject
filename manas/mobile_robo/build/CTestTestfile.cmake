# CMake generated Testfile for 
# Source directory: /home/manas/mobile_robo/src
# Build directory: /home/manas/mobile_robo/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("motion_planing")
subdirs("obstacle_avoiding")
subdirs("wheeled_robot")
