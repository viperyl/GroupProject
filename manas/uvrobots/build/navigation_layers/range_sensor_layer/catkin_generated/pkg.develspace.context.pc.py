# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/manas/uvrobots/devel/include;/home/manas/uvrobots/src/navigation_layers/range_sensor_layer/include".split(';') if "/home/manas/uvrobots/devel/include;/home/manas/uvrobots/src/navigation_layers/range_sensor_layer/include" != "" else []
PROJECT_CATKIN_DEPENDS = "angles;costmap_2d;dynamic_reconfigure;geometry_msgs;pluginlib;roscpp;rospy;sensor_msgs;tf2_geometry_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrange_sensor_layer".split(';') if "-lrange_sensor_layer" != "" else []
PROJECT_NAME = "range_sensor_layer"
PROJECT_SPACE_DIR = "/home/manas/uvrobots/devel"
PROJECT_VERSION = "0.5.0"
