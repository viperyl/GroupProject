# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/manas/uvrobots/src/aruco_ros/aruco_ros/src/aruco_ros_utils.cpp" "/home/manas/uvrobots/build/aruco_ros/aruco_ros/CMakeFiles/marker_publisher.dir/src/aruco_ros_utils.cpp.o"
  "/home/manas/uvrobots/src/aruco_ros/aruco_ros/src/marker_publish.cpp" "/home/manas/uvrobots/build/aruco_ros/aruco_ros/CMakeFiles/marker_publisher.dir/src/marker_publish.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"aruco_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/manas/uvrobots/devel/include"
  "/home/manas/uvrobots/src/aruco_ros/aruco_ros/include"
  "/home/manas/uvrobots/src/vision_opencv/cv_bridge/include"
  "/home/manas/uvrobots/src/aruco_ros/aruco/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/manas/uvrobots/build/vision_opencv/cv_bridge/src/CMakeFiles/cv_bridge.dir/DependInfo.cmake"
  "/home/manas/uvrobots/build/aruco_ros/aruco/CMakeFiles/aruco.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
