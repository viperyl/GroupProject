#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/manas/uvrobots/src/world_canvas/world_canvas_server"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/manas/uvrobots/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/manas/uvrobots/install/lib/python2.7/dist-packages:/home/manas/uvrobots/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/manas/uvrobots/build" \
    "/usr/bin/python2" \
    "/home/manas/uvrobots/src/world_canvas/world_canvas_server/setup.py" \
     \
    build --build-base "/home/manas/uvrobots/build/world_canvas/world_canvas_server" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/manas/uvrobots/install" --install-scripts="/home/manas/uvrobots/install/bin"
