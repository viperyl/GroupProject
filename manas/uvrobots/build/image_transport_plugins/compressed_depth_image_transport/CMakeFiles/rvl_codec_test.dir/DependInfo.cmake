# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/manas/uvrobots/src/image_transport_plugins/compressed_depth_image_transport/test/rvl_codec_test.cpp" "/home/manas/uvrobots/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/rvl_codec_test.dir/test/rvl_codec_test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"compressed_depth_image_transport\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/manas/uvrobots/devel/include"
  "/home/manas/uvrobots/src/image_transport_plugins/compressed_depth_image_transport/include"
  "/home/manas/uvrobots/src/vision_opencv/cv_bridge/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "/usr/src/googletest/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/manas/uvrobots/build/gtest/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/manas/uvrobots/build/image_transport_plugins/compressed_depth_image_transport/CMakeFiles/compressed_depth_image_transport_test.dir/DependInfo.cmake"
  "/home/manas/uvrobots/build/vision_opencv/cv_bridge/src/CMakeFiles/cv_bridge.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
