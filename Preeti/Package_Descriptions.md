# Folder 1: aruco_ros

## aruco >> src >> aruco

### 1) cameraparameters.cpp

Sets different camera parameters like matrix, distortion, extrinsic matrix over a filter 'CamSize' with respect to aruco markers

### 2) cvdrawingutils.cpp

Used to draw lines and shapes in 2d and 3d. Functions include use of opencv platform to draw lines of different lengths (specified by points in the image), different colours, and putting text over the line in the image. 

### 3) debug.cpp

--



### 4) dictionary.cpp

Used to identify the aruco marker from the dictornary of aruco markers. Can detect errors in markers like marker cannot be distinguished, rotations are wrong, etc

### 5) ippe.cpp

IPPE stands for Infinitesimal Plane-based Pose Estimation. It is used to compute 3d objects for pose estimation and scene understanding. Returns 2 pose solutions (1st has lowest error and 2nd can be rejected if the error is significantly worse than 1st. If the object is viewed from a large distance, 2 possible solutions exist w.r.t noise (ambiguous Problem), then the above mentioned 2 solutions are needed.) The code also gives 2 pose solutions and calculates 2 reprojection errors. 

https://github.com/tobycollins/IPPE



### 6) marker.cpp

--



### 7) markerdetector.cpp

Detects the markers in the given image and then detected the position of the marker w.r.t. real locations. Markr detection is detected using drawApproxCurve and drawing rectangles on the border of the aruco marker.

### 8) markerlabeler.cpp

Labels the marker w.r.t the markers detected from the dictionary

### 9) markermap.cpp

--



### 10) posetracker.cpp

Uses the detected markers to localize the position of the robot.



## aruco_ros >> src



### 1) aruco_ros_utils.cpp

Defines the aruco markers w.r.t the parameters of the camera like pose, length of the camera from the image, distortion, etc.

### 2) marker_publish.cpp

Publishes marker to topic. Also, has a for loop for drawing detected markers on image fro visualization, and drawing a 3d cube if the correct information is present. The main loop publishes a ArucoMarker Publisher node.

### 3) simple_double.cpp

Detects the marker, draws a border around that, but only publishes the selected marker. Useful to set parent and child relationship using publisher node between aruco markers and the camera.

### 4) simple_single.cpp

Has the same function as #3 but here the aruco node will publish a marker of size %d and in the double it will publish a size of %d %d.



# Folder 2: hector_slam

## hector_compressed_map_transport

### 1) src >> map_to_image_node.cpp

Converts map to image data and publishes the images using  image_transport. The conversion is only done when the image data is  requested, so the computational cost of this node running is low when no images are requested. Both a full map image as well as a tile based map image that shows only the local area around the robot are made  available. For the latter to work, the robot pose must be available on  the 'pose' topic as described below. 

Subscriber topics: map, pose

Publisher topics: map_image/full, map_image/title

http://wiki.ros.org/hector_compressed_map_transport?distro=melodic



## hector_map_tools >> include >> hector_map_tools

### 1) HectorMapTools.cpp

hector_map_tools contains some functions related to accessing information from OccupancyGridMap maps.    Currently consists of a single header.

http://wiki.ros.org/hector_map_tools?distro=melodic





# Folder 3 : image_transport_plugins

## compressed_depth_image_transport

In general, image transport plugins are used to publish and subscribe images in a compressed png or jpeg format.

`compressed_depth_image_transport` is a plugin package for [image_transport](http://wiki.ros.org/image_transport). It enables any node using `image_transport` classes to publish and subscribe to compressed depth image/map topics

Published Topics: <base_topic>/compressedDepth

Subscriber Topics: <base_topic>/compressedDepth

http://wiki.ros.org/compressed_depth_image_transport?distro=melodic

To use image_transport and the various plugins, tutorial: http://wiki.ros.org/image_transport/Tutorials



## compressed_image_transport

`compressed_image_transport` is a plugin package for [image_transport](http://wiki.ros.org/image_transport). It enables any node using `image_transport` classes to publish and subscribe to compressed image topics.

Publisher Topic: `<base_topic>/compressed` ([sensor_msgs/CompressedImage](http://docs.ros.org/en/api/sensor_msgs/html/msg/CompressedImage.html)) 

 Subscriber Topics: `<base_topic>/compressed` ([sensor_msgs/CompressedImage](http://docs.ros.org/en/api/sensor_msgs/html/msg/CompressedImage.html)) 

Extra Information regarding publishing images directly: Some cameras (particularly webcams) output their image data already in  JPEG format. When writing a driver for such a camera, a quick and dirty  approach is to simply copy the JPEG data into a [sensor_msgs/CompressedImage](http://docs.ros.org/en/api/sensor_msgs/html/msg/CompressedImage.html) message and publish it on a topic of the form `image_raw/compressed`. Then any ROS node using [image_transport](http://wiki.ros.org/image_transport) can subscribe to `image_raw` with transport `compressed`, just as if `image_transport` were used on the publisher side. Of course, the other transport topics (including `image_raw` itself) will not be available. 



http://wiki.ros.org/compressed_image_transport?distro=melodic



## theora_image_transport

Theora_image_transport provides a plugin to image_transport for    transparently sending an image stream encoded with the Theora codec. It enables any node using `image_transport` classes to publish and subscribe to image topics compressed over the wire using the Theora video codec. 

Publisher Topics: `<base topic>/theora` ([theora_image_transport/Packet](http://docs.ros.org/en/api/theora_image_transport/html/msg/Packet.html)) 

Subscriber Topics: `<base topic>/theora` ([theora_image_transport/Packet](http://docs.ros.org/en/api/theora_image_transport/html/msg/Packet.html)) 

Nodes:

1) ogg_saver : `ogg_saver` listens to the Theora transport  packets associated with an image topic and saves them out to a .ogv file for later playback with tools like VLC or mplayer.

   Subscriber Topic: `<image>/theora` ([theora_image_transport/Packet](http://docs.ros.org/en/api/theora_image_transport/html/msg/Packet.html)) 

   Use:

   ````
   $ rosrun theora_image_transport ogg_saver image:=<base_topic> [output_file.ogv]
   ````

   

