# TurtleBot3 in a Gazebo World



```
roscore
```



## Setup Files (Use Everytime you run a world)



``` 
echo "source ~/catkin_ws/devel/setup.bash">> ~/.bashrc
```



```
echo "export TURTLEBOT3_MODEL = burger" >> ~/.bashrc

(Use waffle_pie instead of burger to access the camera onboard the robot for SLAM. Burger does not have a camera onboard)
(Change the TURTLEBOT3_MODEL to whatever robot you want to use)
```



```
source ~/.bashrc
```



## Launching a robot in empty gazebo world



``` 
roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch

(Change the turtlebot3 to whatever robot you want to use if you are not using the turtlebot models)
```



## Operating the turtlebot with keys/remote control



```
roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```



## Creating a map in RViZ



```
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping

(Use this command before the teleop operation to view the map in RViZ. This map is created using the camera available onboard with waffle_pie turtlebot)
```

