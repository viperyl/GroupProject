# CMake generated Testfile for 
# Source directory: /home/preeti/turtlebot/src
# Build directory: /home/preeti/turtlebot/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("turtlebot3_simulations/turtlebot3_simulations")
subdirs("turtlebot3_simulations/turtlebot3_fake")
subdirs("turtlebot3_simulations/turtlebot3_gazebo")
