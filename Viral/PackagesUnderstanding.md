# 1. Lidar Filter.py 

For basic understanding the results of the filter is to update the vaules of the lidar sensor at the moment we are using only two functions i.e. angularFilterOut and minRangeFilter.

**Input = LaserScan(Type of messages = sensor_msgs.msg)**

Functions 

- angularFilterIn

Input - start angle, current angle, start time, filteredScan, lowerAngle, upperAngle

Output - Updated values of filteredScan.header, filteredScan.angle_min, filteredScan.angle_max

- angularFilterOut

The current angle is first compared with the lower angle and upper angle and if the value lies within the range then filtered scan range value is changed to inputscan range + 1. Then then the length filterScan intensities(FSI) is compared with i of input scan range(ISR) if is FSI is less than the ISR the value of FSI is set to zero and the counter is increased by one. And before starting the next iteration the current angle value is change by added itself to inputScan angle_increment.

- minRangeFilter

The filtered scan range(FSR) is compared with the predefined user minRange and if the valus id lesser then that then the FSR values is  changed to inputScan range max +1.  and FSR min is set to minRange.

- update Filter

The function is used to call the above three function to update the values accordingly and then publish them as filteredScan so other nodes can subscribed it to use it in slam and navigation.







# 2. Navigation 

## 2.1 map_server

### 2.1.1  Crop_map (Scripts)

<ins>To much of theory</ins>

A function called find_bound is used to find the bounds of the image(map), it is used to find the x_min, x_end, y_min, y_end. 

Another function called computed_cropped_origin uses the map_image, bounds, resolution and origin, the origin values are stored in variables then the delta is figured out by finding the co-ordinates of lower left corner. Using these values and equation new new ox and oy are calculated, the funnction returns the values of new_ox, new_oy anand the oth. (oth:- theta).

In the main multiple conditions are set starting for if the sys.argv(command line arguments) is less then 2 terminate the loop/exit.  Second condition if the sys.argv[1] as f then the file is loaded and saved as map_data.  Lastly if the sys.argv are greater than 2, then the second argument is saved in variable crop_name. 

It also helps in changing, opening and saving images/updated cropped images.

*-------------------------------------------------------------------------------------------------------------*

From ROS wiki -

map_server provides the `map_server` ROS [Node](http://www.ros.org/wiki/Nodes), which offers map data as a ROS [Service](http://www.ros.org/wiki/Services). It also provides the `map_saver` command-line utility, which allows dynamically generated maps to be saved to file.



# 3. Navigation Layers 

## 3.1 range_sensor_layer



The range_sensor_layer is a plugin for the LayeredCostmap in [costmap_2d](http://wiki.ros.org/costmap_2d), that uses the [sensor_msgs/Range](http://docs.ros.org/en/api/sensor_msgs/html/msg/Range.html) message. It is intended for use with sonar and infrared data. 

The Range messages are integrated into the costmap by using a probabilistic model. 

Cells with a probability higher than the mark_threshold are marked as lethal  obstacles in the master costmap. Cells with a probability lower than the clear_threshold are marked as free space in the master costmap. 



## 3.2 social_navigation_layer

The social layer has two sub-layers called proxemic and passing layer. They subscribe to where the people are and alter the costmaps with a Gaussian distribution around the people.  

The proxemic layer adds gaussian costs all around the detected person,  with the parameters specified above. If the person is stationary, the  gaussian is perfectly round. However, if the person is moving, then the  costs will be increased in the direction of their motion. How far in  front of the person the costs are increased is proportional to the `factor` parameter.  



Passing layer is intended to cause the robot to always pass on the left  side of a person and will hence only increase the cost on one side of  the person.  



