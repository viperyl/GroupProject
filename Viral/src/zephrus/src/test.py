from math import sqrt


def load_location_dict():
    locations = {}
    with open("/home/dviral/v_ws/src/zephrus/src/Cordinates.txt") as myfile:
        for line in myfile:
            components = line.split(' ')
            components[-1] = components[-1][:-1]
            print(components)
            locations[components[0]] = (float(components[1]), float(components[2]), float(components[3]))
    return locations


if __name__ == "__main__":
    task_queue = []
    locations = load_location_dict()
    

