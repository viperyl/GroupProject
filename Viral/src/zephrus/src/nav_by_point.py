import roslib
import rospy
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from random import sample
from math import pow, sqrt
import ast

class Nav():
    def __init__(self):
        rospy.init_node("nav", anonymous=True)

        rospy.on_shutdown(self.shutdown)
        # rest time for robot in each destination
        self.rest_time = rospy.get_param("~rest_time", 10)
        self.fake_test = rospy.get_param("~fake_test", False)

        goal_states = ["PENDING", "ACTIVE", "PREEMPTED", 
                       "SUCCEEDED", "ABORTED", "REJECTED",
                        "PREEMPTING", "RECALLING", "RECALLED",
                        "LOST"]
        locations = {}
        with open("location.txt") as myfile:
            for line in myfile:
                    components = line.split(' ')
                    components[-1] = components[-1][:-1]
                    locations[components[0]] = (float(components[1]), float(components[2]), float(components[3]))
        self.cmd_vel_pub = rospy.Publisher("cmd_vel", Twist)



        
