
"use strict";

let PublishMap = require('./PublishMap.js')
let DetectMoreLoopClosures = require('./DetectMoreLoopClosures.js')
let GetNodeData = require('./GetNodeData.js')
let LoadDatabase = require('./LoadDatabase.js')
let CleanupLocalGrids = require('./CleanupLocalGrids.js')
let SetGoal = require('./SetGoal.js')
let GetPlan = require('./GetPlan.js')
let ListLabels = require('./ListLabels.js')
let SetLabel = require('./SetLabel.js')
let GetNodesInRadius = require('./GetNodesInRadius.js')
let GlobalBundleAdjustment = require('./GlobalBundleAdjustment.js')
let GetMap = require('./GetMap.js')
let AddLink = require('./AddLink.js')
let ResetPose = require('./ResetPose.js')
let GetMap2 = require('./GetMap2.js')
let RemoveLabel = require('./RemoveLabel.js')

module.exports = {
  PublishMap: PublishMap,
  DetectMoreLoopClosures: DetectMoreLoopClosures,
  GetNodeData: GetNodeData,
  LoadDatabase: LoadDatabase,
  CleanupLocalGrids: CleanupLocalGrids,
  SetGoal: SetGoal,
  GetPlan: GetPlan,
  ListLabels: ListLabels,
  SetLabel: SetLabel,
  GetNodesInRadius: GetNodesInRadius,
  GlobalBundleAdjustment: GlobalBundleAdjustment,
  GetMap: GetMap,
  AddLink: AddLink,
  ResetPose: ResetPose,
  GetMap2: GetMap2,
  RemoveLabel: RemoveLabel,
};
