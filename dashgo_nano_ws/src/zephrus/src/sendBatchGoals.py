#!/usr/bin/env python

import os
import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import numpy as np
import sys

def movebase_client(dest):
    # dest: tuple: (x, y, w) x, y posiiton and orientation

    client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    client.wait_for_server()

    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = dest[0]
    goal.target_pose.pose.position.y = dest[1]
    goal.target_pose.pose.orientation.w = dest[2]

    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()


def load_location_dict():
    locations = {}
    with open("/home/viper/Documents/Github/GroupProject/dashgo_nano_ws/src/zephrus/src/Cordinates.txt") as myfile:
        for line in myfile:
            components = line.split(' ')
            components[-1] = components[-1][:-1]
            locations[components[0]] = (float(components[1]), float(components[2]), float(components[3]))
    return locations


def topo_pp(path_seq, locations):
    path_matrix = {item:{} for item in path_seq}
    for sta in path_seq:
        for end in path_seq:
            if sta != end:
                path_matrix[sta][end] = np.sqrt((locations[sta][0] - locations[end][0])**2 + (locations[sta][1] - locations[end][1])**2)

    visited_node = ["start"]
    result= path_matrix["start"]
    del path_matrix["start"]
    unvisited_list = list(path_matrix.keys())
    while len(unvisited_list):
        temp_node = visited_node[-1]
        if temp_node!="start":
            distance_list=[]
            node_list=[]
            for item in unvisited_list:
                distance_list.append(path_matrix[temp_node][item])
                node_list.append(item)
                print(result[temp_node]+path_matrix[temp_node][item])
                if result[item]>result[temp_node]+path_matrix[temp_node][item]:
                    result[item]=result[temp_node]+path_matrix[temp_node][item]

            min_dist=min(distance_list)
            the_index=distance_list.index(min_dist)
            min_node=unvisited_list.pop(the_index)
            visited_node.append(min_node)

        else:
            min_key = list(result.keys())[0]
            min_path=result[min_key]
            for key,value in result.items():
                if value<min_path:
                    min_path=value
                    min_key=key

            visited_node.append(min_key)
            the_index=unvisited_list.index(min_key)
            unvisited_list.pop(the_index)
    return path_matrix


def TaskMode1(taskQueue, locations):
    '''
    robot back to start point first,
    then go to the fisrt desitnion then back to the start
    until taskqueue empty.
    '''
    pass




    
if __name__ == '__main__':
    if len(sys.argv) == 1:
        raise ValueError("0 Task Num")


    for i in range(1, len(sys.argv)):
        print(sys.argv[i])


    
    task_queue = []
    locations = load_location_dict()
    # task_queue.append(locations["start"])
    task_queue.append("Start")

    for i in range(1, len(sys.argv)):
        task_queue.append("Table" + sys.argv[i])
        task_queue.append("Start")

    
    # print(locations["Table10"])
    # task_queue = ["Table1", "Table2"]
    for item in task_queue:
        curDestination = locations[item]
        print(curDestination)
        try:
            rospy.init_node('movebase_client_py')
            result = movebase_client(dest=curDestination)
            if result:
                rospy.loginfo("Goal execution done!")
        except rospy.ROSInterruptException:
            rospy.loginfo("Navigation test finished.")
        rospy.sleep(1)
        
        
    # movebase_client(dest=locations["Start"])
        

