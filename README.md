# Group Project













# Map conversion

prepare your map image file, `.PNG` or `.PGM`. 

publish map file by `map_server` through `.yaml` file.

then run the `map2gazeb.launch` to convert image to 3d map

```bash
cd ./GroupProject/dashgo_nano_ws/mapfile
roscore
rosrun map_server map_server my.yaml
roslaunch map2gazebo map2gazebo.launch
roslaunch map2gazebo gazebo_world.launch

cp /home/viper/Documents/learning_ros/src/map2gazebo/models/map/meshes/map.dae -d /home/viper/.gazebo/models/red_dot_bar/meshes
```

### Navigation with manually set 

```
roslaunch uvrobots_simulations office_world.launch
roslaunch uvrobots_nav gmapping_sim.launch
```

In RviZ

Add a `Map`, rename to `static map`

Add a `pose array`, rename to `particle cloud`

Add a `Polygon`, rename to `Robot footprint`

Assign `local costmap` to `Robot footprint` topic.

Click `2D nav goal` at the top bar, and set a destination.

## Move

### 1 Command line

```
rostopic pub /cmd_vel geometry_msgs/Twist "linear:
  x: 1.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.3" -r 3
```

### 2 Joy

```
roslaunch uvrobots_simulations office_world.launch
roslaunch zephrus example1.launch
roslaunch uvrobots_nav mapping_sim.launch
```



# Work on Red dot Bar map

```
cp -r /home/viper/Documents/Github/GroupProject/yang/red_dot_bar -d  /home/viper/.gazebo/models
roslaunch uvrobots_simulations red_dot_bar_world.launch
```







