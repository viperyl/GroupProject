# File structure

```
├── cv_bridge
│   ├── CHANGELOG.rst
│   ├── cmake
│   │   └── cv_bridge-extras.cmake.in
│   ├── CMakeLists.txt
│   ├── doc
│   │   ├── conf.py
│   │   ├── index.rst
│   │   └── mainpage.dox
│   ├── include
│   │   └── cv_bridge
│   │       ├── cv_bridge.h
│   │       └── rgb_colors.h
│   ├── package.xml
│   ├── python
│   │   ├── CMakeLists.txt
│   │   ├── cv_bridge
│   │   │   ├── core.py
│   │   │   └── __init__.py
│   │   └── __init__.py.plain.in
│   ├── rosdoc.yaml
│   ├── setup.py
│   ├── src
│   │   ├── boost
│   │   │   ├── core
│   │   │   │   └── scoped_enum.hpp
│   │   │   ├── endian
│   │   │   │   ├── conversion.hpp
│   │   │   │   └── detail
│   │   │   │       └── intrinsic.hpp
│   │   │   ├── predef
│   │   │   │   ├── detail
│   │   │   │   │   ├── _cassert.h
│   │   │   │   │   ├── endian_compat.h
│   │   │   │   │   └── test.h
│   │   │   │   ├── library
│   │   │   │   │   └── c
│   │   │   │   │       ├── gnu.h
│   │   │   │   │       └── _prefix.h
│   │   │   │   ├── make.h
│   │   │   │   ├── os
│   │   │   │   │   ├── android.h
│   │   │   │   │   ├── bsd
│   │   │   │   │   │   ├── bsdi.h
│   │   │   │   │   │   ├── dragonfly.h
│   │   │   │   │   │   ├── free.h
│   │   │   │   │   │   ├── net.h
│   │   │   │   │   │   └── open.h
│   │   │   │   │   ├── bsd.h
│   │   │   │   │   ├── ios.h
│   │   │   │   │   └── macos.h
│   │   │   │   ├── other
│   │   │   │   │   └── endian.h
│   │   │   │   └── version_number.h
│   │   │   └── README
│   │   ├── CMakeLists.txt
│   │   ├── cv_bridge.cpp
│   │   ├── module.cpp
│   │   ├── module.hpp
│   │   ├── module_opencv2.cpp
│   │   ├── module_opencv3.cpp
│   │   ├── pycompat.hpp
│   │   └── rgb_colors.cpp
│   └── test
│       ├── CMakeLists.txt
│       ├── conversions.py
│       ├── enumerants.py
│       ├── python_bindings.py
│       ├── test_compression.cpp
│       ├── test_endian.cpp
│       ├── test_rgb_colors.cpp
│       ├── utest2.cpp
│       └── utest.cpp
├── image_geometry
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── doc
│   │   ├── conf.py
│   │   ├── index.rst
│   │   └── mainpage.dox
│   ├── include
│   │   └── image_geometry
│   │       ├── exports.h
│   │       ├── pinhole_camera_model.h
│   │       └── stereo_camera_model.h
│   ├── package.xml
│   ├── rosdoc.yaml
│   ├── setup.py
│   ├── src
│   │   ├── image_geometry
│   │   │   ├── cameramodels.py
│   │   │   └── __init__.py
│   │   ├── pinhole_camera_model.cpp
│   │   └── stereo_camera_model.cpp
│   └── test
│       ├── CMakeLists.txt
│       ├── directed.py
│       └── utest.cpp
├── opencv_tests
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── launch
│   │   └── pong.launch
│   ├── mainpage.dox
│   ├── nodes
│   │   ├── broadcast.py
│   │   ├── rosfacedetect.py
│   │   └── source.py
│   └── package.xml
├── README.rst
└── vision_opencv
    ├── CHANGELOG.rst
    ├── CMakeLists.txt
    └── package.xml
```

# 1. CV_bridge

Interface ROS and OpenCV.

OpenCV save image with `Mat` matrix, ROS stored image with `image.msgs`.

`CV_bridge` provides two types of conversion, `copy` and `share`.

## 1.1 ROS image message to Mat

```c++
   // Case 1: Always copy, returning a mutable CvImage
   CvImagePtr toCvCopy(const sensor_msgs::ImageConstPtr& source,
                       const std::string& encoding = std::string());
   CvImagePtr toCvCopy(const sensor_msgs::Image& source,
                       const std::string& encoding = std::string());
   
   // Case 2: Share if possible, returning a const CvImage
   CvImageConstPtr toCvShare(const sensor_msgs::ImageConstPtr& source,
                             const std::string& encoding = std::string());
   CvImageConstPtr toCvShare(const sensor_msgs::Image& source,
                             const boost::shared_ptr<void const>& tracked_object,
                             const std::string& encoding = std::string());
```

 ## 1.2 Mat to ROS image message

```c++
cv::Mat image = cv::imread(......);
sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
```



## 1.3 Image_geometry

image_geometry contains camera model classes that simplify interpreting images geometrically using the calibration parameters from sensor_msgs/CameraInfo messages. They may be efficiently updated in image callback:

# 2. ydlidar_ros

yd lidar ros interface and development kit.

the point cloud data in the `/camera/depth/points` topic.



# 3. world_canvas

Only support kinetic????

































